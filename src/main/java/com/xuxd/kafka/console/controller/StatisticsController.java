package com.xuxd.kafka.console.controller;

import com.xuxd.kafka.console.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Description:
 * 统计条数
 *
 * @create 2023-07-26
 */
@RestController
@RequestMapping("/statistics")
public class StatisticsController {
    @Autowired
    private StatisticsService statisticsService;
    @GetMapping("/consumerLagCount")
    public Object consumerLagCount() {
        return statisticsService.getConsumerLagCount();
    }


    @GetMapping("/getTopicLagCount")
    public Object getTopicLagCount() {
        return statisticsService.getTopicLagCount();
    }
}
