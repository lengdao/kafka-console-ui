package com.xuxd.kafka.console.service.impl;

import com.xuxd.kafka.console.beans.ResponseData;
import com.xuxd.kafka.console.beans.vo.ConsumerDetailVO;
import com.xuxd.kafka.console.service.StatisticsService;
import kafka.console.ConsumerConsole;
import kafka.console.TopicConsole;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.ConsumerGroupState;
import org.apache.kafka.common.TopicPartition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

/**
 * Description:
 *
 * @create 2023-08-02
 */
@Slf4j
@Service
public class StatisticsServiceImpl implements StatisticsService {
    @Autowired
    private TopicConsole topicConsole;
    @Autowired
    private ConsumerConsole consumerConsole;

    @Override
    public ResponseData getConsumerLagCount() {
        Set<ConsumerGroupState> stateSet = new HashSet<>();
        Set<String> stateGroup = consumerConsole.getConsumerGroupIdList(stateSet);
        if (stateGroup.isEmpty()) {
            return ResponseData.create().data(Collections.emptyList()).success();
        }
        Collection<ConsumerConsole.TopicPartitionConsumeInfo> consumerDetail =
                consumerConsole.getConsumerDetail(stateGroup);
        List<ConsumerDetailVO> collect = consumerDetail.stream().map(ConsumerDetailVO::from).collect(Collectors.toList());
        Map<String, List<ConsumerDetailVO>> map = collect.stream().collect(Collectors.groupingBy(ConsumerDetailVO::getGroupId));
        Map<String, Long> res = new HashMap<>();
        Map<String, Long> finalRes = res;
        map.forEach((groupId, list) -> {
            finalRes.put(groupId, list.stream().map(ConsumerDetailVO::getLag).reduce(Long::sum).get());
        });
        res = sortMapByValues(finalRes);
        return ResponseData.create().data(res).success();
    }
    @Override
    public ResponseData getTopicLagCount() {
        Set<ConsumerGroupState> stateSet = new HashSet<>();
        Set<String> stateGroup = consumerConsole.getConsumerGroupIdList(stateSet);
        if (stateGroup.isEmpty()) {
            return ResponseData.create().data(Collections.emptyList()).success();
        }
        Collection<ConsumerConsole.TopicPartitionConsumeInfo> consumerDetail =
                consumerConsole.getConsumerDetail(stateGroup);
        List<ConsumerDetailVO> collect = consumerDetail.stream().map(ConsumerDetailVO::from).collect(Collectors.toList());
        Map<String, List<ConsumerDetailVO>> map = collect.stream().collect(Collectors.groupingBy(ConsumerDetailVO::getTopic));
        Map<String, Long> res = new HashMap<>();
        Set<String> topicSet = topicConsole.getTopicNameList(false);
        for (String topic : topicSet) {
            List<ConsumerDetailVO> list = map.get(topic);
            if (null == list || list.isEmpty()) {
                res.put(topic, 0L);
            } else {
                res.put(topic, list.stream().map(ConsumerDetailVO::getLag).reduce(Long::sum).get());
            }
        }
        res = sortMapByValues(res);
        return ResponseData.create().data(res).success();
    }



    /**
     * 根据map中的value大小进行排序【由大到小】
     */
    public static <K extends Comparable,V extends Comparable> Map<K, V> sortMapByValues(Map<K, V> map){
        //需要用LinkedHashMap排序
        HashMap<K, V> finalMap = new LinkedHashMap<K, V>();
        //取出map键值对Entry<K,V>，然后按照值排序，最后组成一个新的列表集合
        List<Map.Entry<K, V>> list = map.entrySet()
                .stream()
                //sorted((p2,p1)   表示由大到小排序   ||  sorted((p1,p2)   表示由小到大排序
                .sorted((p2,p1)->p1.getValue().compareTo(p2.getValue()))
                .collect(Collectors.toList());
        //遍历集合，将排好序的键值对Entry<K,V>放入新的map并返回。
        list.forEach(ele->finalMap.put(ele.getKey(), ele.getValue()));
        return finalMap;
    }
}
