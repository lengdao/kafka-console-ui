package com.xuxd.kafka.console.service;

import com.xuxd.kafka.console.beans.ResponseData;

/**
 * Description:
 *
 * @create 2023-07-26
 */
public interface StatisticsService {
    /***
     * 消费组积压情况
     * @return
     */
    ResponseData getConsumerLagCount();

    /**
     * Topic积压情况
     * @return
     */
    ResponseData getTopicLagCount();

}
